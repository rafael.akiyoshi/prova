package Codigo;

public class Opcao {
	
	private String opcao1;
	private int voto1;
	private String opcao2;
	private int voto2;
	private String opcao3;
	private int voto3;
	public String getOpcao1() {
		return opcao1;
	}
	public void setOpcao1(String opcao1) {
		this.opcao1 = opcao1;
	}
	public int getVoto1() {
		return voto1;
	}
	public void setVoto1(int voto1) {
		this.voto1 = voto1;
	}
	public String getOpcao2() {
		return opcao2;
	}
	public void setOpcao2(String opcao2) {
		this.opcao2 = opcao2;
	}
	public int getVoto2() {
		return voto2;
	}
	public void setVoto2(int voto2) {
		this.voto2 = voto2;
	}
	public String getOpcao3() {
		return opcao3;
	}
	public void setOpcao3(String opcao3) {
		this.opcao3 = opcao3;
	}
	public int getVoto3() {
		return voto3;
	}
	public void setVoto3(int voto3) {
		this.voto3 = voto3;
	}
	
	

}
