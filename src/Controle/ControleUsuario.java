package Controle;
import java.util.ArrayList;

import Codigo.*;

public class ControleUsuario {


	public ControleUsuario() {
		// TODO Auto-generated constructor stub
	}


	private ArrayList<Topico> listaTopicos;
	
	public String adicionarTopico(Topico umTopico){
		listaTopicos.add(umTopico);
		return "Topico adicionado! Aguarde Liberacao";
		
	}
	
	public String curtirTopico(Topico umTopico){
		int curtidas = umTopico.getCurtidas();
		int curtidasNovas = curtidas++;
		return "Obrigado por Curtir!";
	}
	
	public String votarEnquete(Opcao umaOpcao){
		int votos1 = umaOpcao.getVoto1();
		int votos2 = umaOpcao.getVoto2();
		int votos3 = umaOpcao.getVoto3();
		return "Obrigado por votar!";
	}
	public String aceitarTopico(Topico umTopico){
		umTopico.setAceito(true);
		return "Topico aceito!";
		
	}
	
	public String removerTopico(Topico umTopico){
		getListaTopicos().remove(umTopico);
		return "Topico removido!";
	}
	
	public String darCredito(Usuario umUsuario){
		int creditos = umUsuario.getCredito();
		int creditosAtuais = creditos++;
		return "Creditos adicionados ao Usuario";
	}

	public ArrayList<Topico> getListaTopicos() {
		return listaTopicos;
	}

	public void setListaTopicos(ArrayList<Topico> listaTopicos) {
		this.listaTopicos = listaTopicos;
	}

}
