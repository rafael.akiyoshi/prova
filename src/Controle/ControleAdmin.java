package Controle;

import java.util.ArrayList;

import Codigo.*;

public class ControleAdmin extends ControleUsuario{
	
	public ControleAdmin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String aceitarTopico(Topico umTopico){
		umTopico.setAceito(true);
		return "Topico aceito!";
		
	}
	
	public String removerTopico(Topico umTopico){
		getListaTopicos().remove(umTopico);
		return "Topico removido!";
	}
	
	public String darCredito(Usuario umUsuario){
		int creditos = umUsuario.getCredito();
		int creditosAtuais = creditos++;
		return "Creditos adicionados ao Usuario";
	}

}
