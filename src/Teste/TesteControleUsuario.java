package Teste;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import Codigo.Topico;
import Controle.ControleUsuario;

public class TesteControleUsuario {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAdicionarTopico() {
		ControleUsuario umControle = new ControleUsuario();
		Topico umTopico = new Topico(null, "Protesto");
		assertEquals(umControle.adicionarTopico(umTopico), "Topico adicionado! Aguarde Liberacao");
	}
	
	@Test
	public void testAceitarTopico() {
		ControleUsuario umControle = new ControleUsuario();
		Topico umTopico = new Topico(null, "Protesto");
		assertEquals(true, umControle.aceitarTopico(umTopico));
	}

}
