package Teste;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Codigo.Topico;
import Controle.ControleAdmin;

public class TesteControleAdmin {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAceitarTopico(Topico umTopico) {
		ControleAdmin umControleAdmin = new ControleAdmin();
		Assert.assertEquals(true, umControleAdmin.aceitarTopico(umTopico));
	}

}
